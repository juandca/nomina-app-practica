package com.nomina.app.plantilla.controller;

import com.nomina.app.plantilla.model.entity.Practica;
import com.nomina.app.plantilla.model.repository.IPracticaDao;
import com.nomina.app.plantilla.model.service.IPracticaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PracticaController {
  @Autowired
  private IPracticaDao practicaDao;
  @Autowired
  private IPracticaService practicaService;

  @GetMapping("/listar")
  public List<Practica> listarPracticas(){
    return practicaService.findByAll();
  }

  @GetMapping("/ver/{id}")
  public Practica findById(Long id){
    return practicaService.findById(id);
  }

  @ResponseStatus(HttpStatus.CREATED)
  @PostMapping("/crear")
  public Practica create(@RequestBody Practica practica){
    return practicaService.create(practica);
  }

  @ResponseStatus(HttpStatus.CREATED)
  @PutMapping("/editar/{id}")
  public Practica edit(@PathVariable Long id, @RequestBody Practica practica){
    return practicaService.edit(id, practica);
  }

  @ResponseStatus(HttpStatus.NO_CONTENT)
  @DeleteMapping("/eliminar/{id}")
  public void delete(@PathVariable Long id){
    practicaService.delete(id);
  }
}
