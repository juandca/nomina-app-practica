package com.nomina.app.plantilla.model.repository;

import com.nomina.app.plantilla.model.entity.Practica;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPracticaDao extends CrudRepository<Practica,Long> {
}
