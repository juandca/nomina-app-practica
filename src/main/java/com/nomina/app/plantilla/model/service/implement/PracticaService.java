package com.nomina.app.plantilla.model.service.implement;

import com.nomina.app.plantilla.model.entity.Practica;
import com.nomina.app.plantilla.model.repository.IPracticaDao;
import com.nomina.app.plantilla.model.service.IPracticaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
public class PracticaService implements IPracticaService {
  @Autowired
  private IPracticaDao practicaDao;

  private Practica practica;

  @Override
  @Transactional(readOnly = true)
  public List<Practica> findByAll(){
    List<Practica> practicas = (List<Practica>) practicaDao.findAll();
    return practicas;
  }

  @Override
  @Transactional(readOnly = true)
  public Practica findById(Long id){
    practica = practicaDao.findById(id).orElse(null);
    return this.practica;
  }

  @Override
  @Transactional(propagation = Propagation.NEVER)
  public Practica create(Practica practica){
    Practica crear = null;
    practica.setFechaRegistro(LocalDate.now());
    // Esto lo seguimos después
    crear = practicaDao.save(practica);
    return crear;
  }

  @Override
  @Transactional(propagation = Propagation.NEVER)
  public Practica edit(Long id, Practica practica){
    practica = this.practicaDao.findById(id).orElse(null);
    practica.setFechaRegistro(LocalDate.now());
    practica.setNombreUsuario(practica.getNombreUsuario());
    practica.setDocumento(practica.getDocumento());
    return practicaDao.save(practica);
  }

  @Override
  @Transactional(propagation = Propagation.NEVER)
  public void delete(Long id){
    practicaDao.deleteById(id);
  }
}
