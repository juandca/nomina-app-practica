package com.nomina.app.plantilla.model.service;

import com.nomina.app.plantilla.model.entity.Practica;

import java.util.List;

public interface IPracticaService {
  List<Practica> findByAll();

  Practica findById(Long id);

  Practica create(Practica practica);

  Practica edit(Long id, Practica practica);

  void delete(Long id);
}
