package com.nomina.app.plantilla.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "practicas")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Practica implements Serializable {
  private static final long serialVersionUID = 2385655326605717769L;
  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;

  @Column(name = "nombre_usuario")
  private String nombreUsuario;

  @Column(unique = true)
  private Integer documento;

  @Column(name = "fecha_registro")
  private LocalDate fechaRegistro;
}
