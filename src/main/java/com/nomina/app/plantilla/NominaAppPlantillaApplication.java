package com.nomina.app.plantilla;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NominaAppPlantillaApplication {

  public static void main(String[] args) {
    SpringApplication.run(NominaAppPlantillaApplication.class, args);
  }

}
